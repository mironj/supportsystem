@extends('layout')

@section('content')

<div class="container">
    
    <a href="/backend" class='m-3'> Back to ticket queue</a>

    <div class="border p-3 rounded">
        <ul class="list-inline">
            <li class="list-inline-item">#{{ $data[0]->id }}</li>
            <li class="list-inline-item">Status: @if($data[0]->replied) <b>{{ 'Closed' }}</b> @else <b>{{ 'Open' }}</b> @endif</li>
            <li class="list-inline-item">Date: {{ \Carbon\Carbon::parse($data[0]->created_at)->format('d/m/Y H:i') }}</li>
            <li class="list-inline-item">Name: {{ $data[0]->customer_name }}</li>
        </ul>
        <ul class="list-inline">
            <li class="list-inline-item">Email: {{ $data[0]->email }}</li>
            <li class="list-inline-item">Tel: {{ $data[0]->telephone }}</li>
            <li class="list-inline-item">Ref: {{ $data[0]->reference }}</li>
        </ul>
    </div>

    <p class="lead mt-3">
        {{ $data[0]->problem_description }}
    </p>

    @if($data[0]->replied==0)
    <form method="POST" action="/backend/{{ $data[0]->id }}">
    @csrf
        <div class="form-group">
            <label >Your Reply:</label>
            <textarea class="form-control" name="reply" rows="3"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Send Reply</button>
    </form>
    @else
      Reply:
      <div class="border p-3 rounded">
        {{ $data[1]->response }}
      </div>  
      Replied By: {{ $data[2]->name }}
    
      
    @endif
    
</div>

@endsection