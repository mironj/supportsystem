@extends('layout')

@section('content')

<h4>Ticket Queue</h4> <span style="float:right">Logged in as: <a href="/home">{{ Auth::user()->name }}</a></span> 
<br>
<div class="container p-3">
    <form method="GET" action="/backend" >
        
        <div class="input-group">
            <input type="text" class="form-control" name="search"
                placeholder="search by name"> 
             <input type="submit" value="search">   
        </div>
    </form>
</div>
<div class="table-responsive-sm">
<table class="table table-hover">
  
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Customer Name</th>
      <th scope="col">Status</th>
      <th scope="col">Email</th>
      <th scope="col">Created Date</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    @foreach($tickets as $ticket)
    <tr class="@if($ticket->replied==0) {{ 'table-primary' }} @endif ">
    <th scope="row">{{ $ticket->id }}</th>
      <td>{{ $ticket->customer_name }}</td>
      <td>@if($ticket->replied==0) {{ 'Open' }} @else {{ 'Replied' }} @endif</td>
      <td>{{ $ticket->email }}</td>
      <td>{{ \Carbon\Carbon::parse($ticket->created_at)->format('d/m/Y H:i') }}</td>
      <td><a href="\backend\{{ $ticket->id }}">@if($ticket->replied==0) {{ 'Respond' }} @else {{ 'View' }} @endif</a></td>
    </tr>

    @endforeach
    
  </tbody>
</table>
</div>

<div class="row">
    <div class="col-12 text-center">
        {{ $tickets->links() }}
    </div>
</div>

@endsection



