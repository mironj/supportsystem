@component('mail::message')

# Your reference number for the opened support ticket

Thank you for opening the support ticket, please visit 
http://support.com/check and enter the refernce number below
to check for the status of your ticket anytime.

# Ticket Reference Number: {{ $reference }}

Thanking you

Support Team

@endcomponent