@extends('layout')


@section('content')        
<h4>Please raise your support ticket by filling the the form below:</h4>

<form method="POST" action="/tickets">

@csrf

<div class="form-group">
    <label for="customer_name">Customer Name</label>
    <input 
        type="text" 
        class="form-control" 
        name="customer_name" 
        value="{{ old('customer_name') }}">
    
    @error('customer_name')
        <small class="form-text text-danger">{{ $message }}</small>
    @enderror

</div>

<div class="form-group">
    <label for="problemDproblem_descriptionescription">Problem Description</label>
    <textarea   
        class="form-control" 
        name="problem_description" 
        rows="3">{{ old('problem_description') }}</textarea>
    
    @error('problem_description')
        <small class="form-text text-danger">{{ $message }}</small>
    @enderror

</div>

<div class="form-group">
    <label for="email">Email</label>
    <input  
        type="text" 
        class="form-control" 
        name="email"  
        value="{{ old('email') }}">
   
    @error('email')
        <small class="form-text text-danger">{{ $message }}</small>
    @enderror

</div>

<div class="form-group">
    <label for="telephone">Telephone</label>
    <input  
        type="text"  
        class="form-control" 
        name="telephone" 
        value="{{ old('telephone') }}">
    
    @error('telephone')
        <small class="form-text text-danger">{{ $message }}</small>
    @enderror

</div>

<button type="submit" class="btn btn-primary">Submit Ticket</button>

</form>
@endsection
    


