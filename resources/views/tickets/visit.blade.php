@extends('layout')


@section('content')   



@if(session('message'))
    <p>Thank you for submiting the support ticket, we have sent you an email with the ticket reference number to view our response.</p>
@else
    <p>Welcome to the online support system.</p> 
    <p>If you wish to raise a new support ticket please click the button below.</p >
    <form method="GET" action="/tickets/create">
    <button type="submit" class="btn btn-primary">Raise a new support ticket</button>
    </form>

    <p class="pt-3">Or if you have already raised a ticket you can view the status of it by clicking the button below</p >
    <form method="GET" action="/tickets/check">
    <button type="submit" class="btn btn-primary">Check status of a support ticket</button>
    </form>
@endif

<br><br><br><br><br><br><br><br><br><br>
<a href='/'>Home</a>  | <a href='/backend'>Backend</a> 



@endsection
    


