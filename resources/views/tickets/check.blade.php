@extends('layout')


@section('content')   



<div class="form-group">
    <label for="customer_name">Enter Reference Number of your ticket</label>
    <input 
        type="text" 
        id="reference_textbox"
        class="form-control" 
        name="referece" 
        value="{{ old('referece') }}">
    
    @error('customer_name')
        <small class="form-text text-danger">{{ $message }}</small>
    @enderror

</div>

<button id="check_button" class="btn btn-primary">Check Status</button>

<i>This page is populated using AJAX</i>

<div class="border p-3 rounded mt-3" style="visibility:hidden" id="ticket_pane">
    <ul class="list-inline">
        <li class="list-inline-item" id="status" style="font-weight:bold"></li>    
        <li class="list-inline-item" id="customer_name"></li>
        <li class="list-inline-item" id="email"></li>
        <li class="list-inline-item" id="telephone"></li>
    </ul>

    <p class="lead mt-3" id="problem_description"></p>
    
</div>

<div class="border p-3 rounded mt-3" style="visibility:hidden" id="response_pane">

    <p class="lead mt-3" id="response"></p>
    
</div>


@endsection

@section('ajax')

<script>

$( document ).ready(function() {
    
    $("#check_button").on("click", function(){

        $("#ticket_pane").css('visibility', 'hidden');
        $("#response_pane").css('visibility', 'hidden');
        
        const reference = $("#reference_textbox").val();

        $.get( "/api/ticket", { reference: reference } )
        .done(function( data ) {
            //alert( "Data Loaded: " + data );
            if(data!='') $.fn.populate_ticket(data);
            
        });

    });

    $.fn.populate_ticket = function (data) {

        if(data[0].customer_name!=''){
        
            $("#ticket_pane").css('visibility', 'visible');

            $("#customer_name").html('Name: ' + data[0].customer_name);
            $("#email").html('Email: ' + data[0].email);
            $("#telephone").html('Telephone: ' + data[0].telephone);
            var status = '';
            if(data[0].replied == 1) status = 'Closed';
            else status = 'Open';
            $("#status").html('Status: ' + status);
            $("#problem_description").html('Problem Description: <br/>' + data[0].problem_description);
        }else $("#ticket_pane").css('visibility', 'hidden');
        
        if(data[1].response!=undefined){

            $("#response_pane").css('visibility', 'visible');
            
            $("#response").html('The Reply: <br/>' + data[1].response);

        }else $("#response_pane").css('visibility', 'hidden');

    };
    

});

</script>

@endsection