<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'CustomerTicketController@visit');
Route::post('/tickets', 'CustomerTicketController@store');
Route::get('/tickets/create', 'CustomerTicketController@create');
Route::get('/tickets/check', 'CustomerTicketController@check');

Route::get('/backend/', 'BackendResponseController@index');
Route::post('/backend/', 'BackendResponseController@index');
Route::get('/backend/{ticket}', 'BackendResponseController@show');
Route::post('/backend/{ticket}', 'BackendResponseController@store');

// Route for AJAX call to check status of a ticket with a reference number
Route::get('/api/ticket', 'CustomerTicketController@status');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/home', function(){
//     return redirect('/backend');
// });
