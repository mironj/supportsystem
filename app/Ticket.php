<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = ['customer_name', 'problem_description', 'email', 'telephone', 'reference', 'replied'];

    public function responses()
    {
        return $this->hasMany('App\Response');
    }

}
