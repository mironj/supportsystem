<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendReference;

class CustomerTicketController extends Controller
{
    public function visit()
    {
        return view('tickets.visit');
    }

    public function create()
    {
        return view('tickets.create');
    }

    public function store()
    {
        // Validating the new ticket request
        request()->validate([
            'customer_name' => 'required|min:3|max:255',
            'problem_description' => 'required|min:3|max:1000',
            'email' => 'required|email',
            'telephone' => 'required|numeric|min:9'
        ]); 
        
        // Generate unique reference number
        $reference = $this->generateReferenceNumber();

        Ticket::create([
            'customer_name' => request('customer_name'),
            'problem_description' => request('problem_description'),
            'email' => request('email'),
            'telephone' => request('telephone'),
            'reference' => $reference
        ]);

        Mail::to(request('email'))
            ->send(new SendReference($reference));

        return redirect('/')->with('message','success');
    }

    protected function generateReferenceNumber()
    {
        // Generate Unique Reference number, check DB to make sure it doesn't match a record
        $reference = rand(10000000,100000000);   
        while(Ticket::where('reference', '=', $reference  )->count() > 0) {
            $reference = rand(10000000,100000000); // generate another reference number if a record exists    
        }  
        return $reference;
    }

    public function check()
    {
        
        return view('tickets.check');
    }
    
    // this is a response to the AJAX request
    public function status() 
    {
        if(request('reference') != null && is_integer((int)request('reference') ))
        {
           $ticket = Ticket::where('reference', '=' ,request('reference'))->first();
           
           if($ticket->replied == 1) 
                $response = $ticket->responses()->first();
            else $response = "";

           return array($ticket, $response);
        }

        return '';
        
    }
    
}
