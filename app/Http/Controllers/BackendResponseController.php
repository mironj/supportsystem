<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Ticket;
use App\Response;
use App\Mail\SendReply;

class BackendResponseController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        if(request('search')!=null){
            $search = request('search');
            $tickets = DB::table('tickets')->where('customer_name','LIKE', '%'.$search.'%')->paginate(10);
        }else{
            $tickets = DB::table('tickets')->latest()->paginate(10);
        }

        

        return view('responses.index', ['tickets' => $tickets]);       
    }

    public function show(Ticket $ticket)
    {
        $response= $ticket->responses()->first();

        if($ticket->replied == 1) 
            $user = $response->user()->first();
        else $user = "";

        $data = array($ticket, $response, $user );
        
        return view('responses.show', compact('data'));
    }

    public function store(Ticket $ticket)
    {
        request()->validate([
            'reply' => 'required'
        ]);

        Response::create([
            'ticket_id' => $ticket->id,
            'response' => request('reply'),
            'user_id' => auth()->user()->id
        ]);

        Mail::to($ticket->email)
            ->send(new SendReply($ticket->reference, $ticket->problem_description, request('reply')));

        $ticket->update([
            'replied' => 1
        ]);

        return redirect('/backend/'.$ticket->id);
    }
}
