<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendReply extends Mailable
{
    use Queueable, SerializesModels;

    public $reference;
    public $problem;
    public $reply;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($reference, $problem, $reply)
    {
        $this->reference = $reference;
        $this->problem = $problem;
        $this->reply = $reply;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.send-reply')
                    ->subject('Here is the reply to your support ticket');
    }
}
