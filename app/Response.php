<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $fillable = ['ticket_id', 'response', 'user_id'];

    public function ticket()
    {
        return $this->belongsTo('App\ticket');
    }

    public function user()
    {
        return $this->belongsTo('App\user');
    }
}
