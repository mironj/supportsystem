<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Ticket;
use Faker\Generator as Faker;

$factory->define(Ticket::class, function (Faker $faker) {
    return [
        'customer_name' => $faker->name,
        'problem_description' => $faker->sentence,
        'email' => $faker->unique()->safeEmail,
        'telephone' => $faker->phoneNumber,
        'reference' => rand(10000000, 100000000),
        'created_at' => now()
    ];
});
